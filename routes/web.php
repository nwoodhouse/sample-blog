<?php

use App\Post;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
| 
*/

// Routes map URIs to 
// URI (http://localhost/doX) -> Action (Controller@method)
// 
// Note the practise of naming routes. Wherever possible, all URLs
// to pages in your project should be dynamically generated using
// a route name - rather than being statically defined.

// The following individual routes are left in for reference for when 
// you need to define your own routes. However, if using a resourceful
// controller, you do not need to declare each route individually - 
// declare them in a single command using the Route::resource(...) statement

// Route::get('posts', 'PostController@index')->name('posts.index');

// show the update form...
// Route::get('posts/create', 'PostController@create')->name('posts.create');

// handle the create post form
// Route::get('posts/{post}', 'PostController@show')->name('posts.show')->where('post', '[0-9]*');

// handle the form submission
// Route::post('posts', 'PostController@store')->name('posts.store');

// No need for all of the individually declared routes above! Wire up all CRUD routes
// using a single statement!
Route::resource('posts', 'PostController');

// the following route allows you to see how a Vue.js component would render the data
Route::get('posts-vue-example', 'PostController@indexUsingVue')->name('posts-vue.index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

