<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

// In general, a controller does the following:
// 1. Triggered by HTTP requests (receive client requests).
// 2. Determine the models to fetch / and or manipulate
// 3. Feed a fiew, with appropriate models
// 
// Usually, where you have a model (E.g., Post), you have 
// a controller to manage that model (E.g., PostController)

class PostController extends Controller
{
	// Actions in controllers are methods that are invoked 
	// in connection with an HTTP request.
    public function index()
    {
    	// extract the posts from the database!
		$posts = Post::paginate(10);
		return view('posts', compact('posts'));
    }

    // The following action returns a slightly different view
    // template that illustrates how to use Vue.js
    public function indexUsingVue()
    {
        // just to keep things manageable - restrict the set returned to just 100 items
        $posts = Post::limit(100)->get();

        // since the view requires the user relationship loaded in eagerly (rather than
        // loaded lazily as needed), we will get Laravel to lookup the users that
        // these posts relate to
        $posts->load('user');

        // now return the view that will display the results
        return view('posts-vue', compact('posts'));
    }

    // Method for showing a single post. Note the type hinting,
    // combined with the parameter name ('post') exactly matching
    // the name of the parameter in the route mapping (in web.php).
    // This allows the system to automatically treat the parameter
    // as the primary key (id) of the given model type (Post) and
    // lookup the model for you, and pass that object to your function.
    public function show(Post $post)
    {
    	return view('post', compact('post'));
    }

    // returns the form through which a Post details can be captured
    // and POST'd to the server for handling
    public function create()
    {
        return view('post-create');
    }

    // handles the form post, from the 'create' form
    public function store(Request $request)
    {
        // ensure that the request has OK input params
        $this->validate($request, [
            'title' => 'required|min:3|max:100',
            'text' => 'required|min:3'
        ]);

        // extract the information from the request
        $values = $request->only('title', 'text');

        $post = $request->user()->posts()->create($values);

        // redirect the user to the details page for this post!
        return redirect( $post->url );
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('posts.index')->with('message', 'Deleted!');
    }
}
