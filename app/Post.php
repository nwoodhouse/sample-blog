<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
    	'title',
    	'text',
    	'user_id'
    ];

    protected $appends = [
        'url'
    ];

    // Link to the user that authored this post.
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // Define a dynamic attribute named 'url'.
    // Dynamic attributes are derrived from a function call, but may be accessed
    // in the same way as regular properties. They come in useful when you have
    // properties that you wish to send from server to client when implementing a
    // data API, and for aesthetic reasons.
    public function getUrlAttribute()
    {
        return route('posts.show', $this->id); 
    }
}
