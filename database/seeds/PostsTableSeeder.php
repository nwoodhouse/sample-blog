<?php

use App\Post;
use App\User;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create a few posts linked to my users...
        foreach(User::all() as $user)
        {
        	// author a random number of posts
        	$numPostsToAuthor = rand(1000, 2000);
        	while($numPostsToAuthor--)
        	{
        		$user->posts()->create([
        			'title' => "Title $numPostsToAuthor",
        			'text' => "This is the post text $numPostsToAuthor"
    			]);
        	}
        }
    }
}
