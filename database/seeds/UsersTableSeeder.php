<?php

use App\User;
use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// create a user in memory
        $user = new User;
        $user->email = 'nathan@calibrae.com';
        $user->name = 'Nathan';
        $user->password = Hash::make('secret');
        
        // now tell the DB to write this user to the DB
        $user->save();

        User::create([
        	'name' => 'Dave',
        	'email' => 'dave@calibrea.com',
        	'password' => Hash::make('secret')
    	]);
    }
}
