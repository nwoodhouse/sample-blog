{{-- This view is responsible for displaying the details of a single post --}}

@extends('layouts.master')

@section('content')
	

	<h1>
		Post Details: {{ $post->id }}

		<form action="{{ $post->url }}" method="post">
			{{ method_field('delete') }}

			<button class="btn btn-danger">
				<i class="glyphicon glyphicon-trash"></i>
				Delete me
			</button>
		</form>
	</h1>

	<div class="panel panel-default">

		<div class="panel-heading">
			{{ $post->title }}
		</div>

		<div class="panel-body">
			{{ $post->text }}
		</div>

		<div class="panel-footer">
			<i class="glyphicon glyphicon-calendar"></i>
			{{ $post->created_at->diffForHumans() }} by {{ $post->user->name }}
		</div>
	</div>

	<a href="{{ route('posts.index') }}">Back to all posts</a>
@endsection