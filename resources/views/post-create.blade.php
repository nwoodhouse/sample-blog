@extends('layouts.master')

@section('content')
	
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	<form method="POST" action="{{ route('posts.store') }}">
		<!-- Need to ensure that a CSRF token (?!?!?) is included -->
		{{ csrf_field() }}

		<div class="form-group @if($errors->has('title')) has-error @endif">
			<label for="title">Title</label>
			<input type="text" class="form-control" id="title" name="title" placeholder="Enter a title for your post" value="{{ old('title') }}">
		</div>
		<div class="form-group @if($errors->has('text')) has-error @endif">
			<label for="text">Type your post content</label>
			<textarea class="form-control" name="text" id="text" rows="3" placeholder="Type your blog post here">{{ old('text') }}</textarea>
		</div>
		
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>

@endsection