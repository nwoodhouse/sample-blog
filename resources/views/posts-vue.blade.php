{{-- This template is responsible for displaying a collection of posts --}}

@extends('layouts.master')

@section('content')

	@if (session('message'))
	    <div class="alert alert-success">
	        {{ session('message') }}
	    </div>
	@endif

	<h1>
		All {{ count($posts) }} posts

		<a href="{{ route('posts.create') }}" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Create</a>
	</h1>

	<div id="posts-app">
		<div class="panel panel-default" v-for="(post, index) in posts">
			<div class="panel-body">
				{{-- Templates should only know what they need to! Note the use of a dynamic attr --}}
				<a v-bind:href="post.url">
					@{{ post.title }}
				</a>

				<small>
					<i class="glyphicon glyphicon-calendar"></i>
					@{{ post.created_at }} by @{{ post.user.name }}
				</small>
				
				<button @click="deletePost(post, index)" class="btn btn-danger btn-xs pull-right">
					<i class="glyphicon glyphicon-trash"></i>
				</button>
				
			</div>
		</div>
	</div>
	
@endsection

@section('scripts')
	<script>

		var myApp = new Vue({
			el: '#posts-app',
			data: {
				posts: {!! $posts !!},
			},
			methods: {
				deletePost: function (post, index) {

					// since we will need a reference to 'this' in a handler
					// that will be bound to a different 'this', store a reference
					// to the right 'this' so that we can reference the this.posts
					// later
					var that = this;

					// confirm the users' decision
					if (confirm('Are you sure you want to delete this post?'))
					{
						// they clicked to delete - send a request to the server to perform the deletion
						$.post(post.url, {
							_method: 'delete'
						}, function (response) {

							// server has deleted the item - remove it from the local view
							that.posts.splice(index, 1);

						}).fail(function (response) {
							// this function is called if something went wrong in deleting a post :-(
							alert('Something went wrong deleting the post!');
						});
					}
				}
			}
		});


	</script>
@endsection