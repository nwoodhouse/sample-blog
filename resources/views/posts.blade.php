{{-- This template is responsible for displaying a collection of posts --}}

@extends('layouts.master')

@section('content')

	@if (session('message'))
	    <div class="alert alert-success">
	        {{ session('message') }}
	    </div>
	@endif

	<h1>
		All {{ count($posts) }} posts

		<a href="{{ route('posts.create') }}" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Create</a>
	</h1>

	{{-- Generates the default (Bootstrap) pagination links --}}
	{{ $posts->links() }}

	@foreach($posts as $post)
		<div class="panel panel-default">
			<div class="panel-body">
				{{-- Templates should only know what they need to! Note the use of a dynamic attr --}}
				<a href="{{ $post->url }}">
					{{ $post->title }}	
				</a>
				-
				<small>
					<i class="glyphicon glyphicon-calendar"></i>
					{{ $post->created_at->diffForHumans() }} by {{ $post->user->name }}
				</small>
			</div>
		</div>
	@endforeach
@endsection